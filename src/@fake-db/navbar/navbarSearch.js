import mock from "../mock"

export const searchResult = [
  {
    groupTitle: "Pages",
    searchLimit: 4,
    data: [
      {
        id: 1,
        target: "home",
        title: "Home",
        link: "/",
        icon: "Home"
      },
      {
        id: 2,
        target: "page",
        title: "Page 2",
        link: "/page2",
        icon: "File"
      },
      {
        id: 3,
        target: "dashboard",
        title: "Dashboard",
        link: "/dashboard",
        icon: "File"
      }
    ]
  },
  {
    groupTitle: "Website",
    searchLimit: 4,
    data: [
      {
        id: 11,
        target: "home",
        title: "Youtube",
        link: "/",
        icon: "Home"
      },
      {
        id: 12,
        target: "page",
        title: "Instagram",
        link: "/page2",
        icon: "File"
      },
      {
        id: 13,
        target: "dashboard",
        title: "Facebook",
        link: "/dashboard",
        icon: "File"
      }
    ]
  }
]

mock.onGet("/api/main-search/data").reply(200, {
  searchResult
})
